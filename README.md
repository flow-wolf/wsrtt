# WebSocket Round Trip Time

The two scripts are designed to test the round trip time for websocket message to go from client to server and back, similar to an ICMP ping test. The message sizes and number of times they are sent can be specified. The client will output to screen and CSV file in the same directory.

### Expected Output

```
$ python3 ws_client.py -p 80 -s 1024000 -d ec2-52-47-143-2.eu-west-3.compute.amazonaws.com -r 10 -w 1
### opened ###
=====================================================================================
msg id     bytes                c_sent               c_recvd              RTT                 
=====================================================================================
0          1024000              1664979388.20149     1664979389.15717     0.95568             
1          1024000              1664979389.52142     1664979390.41658     0.89516             
2          1024000              1664979390.94537     1664979391.76209     0.81672             
3          1024000              1664979392.26416     1664979393.07882     0.81466             
4          1024000              1664979393.60691     1664979394.41442     0.80751             
5          1024000              1664979394.92887     1664979395.74693     0.81806             
6          1024000              1664979396.25208     1664979397.07579     0.82371             
7          1024000              1664979397.56486     1664979398.38522     0.82036             
8          1024000              1664979398.88384     1664979399.69409     0.81025             
9          1024000              1664979400.22584     1664979401.02879     0.80295             
=====================================================================================
### closed ###
thread terminating...

```

### Prerequisites
- Python3
- Pip3 
- [requests](https://pypi.org/project/requests/) - Module needed for the client to get own IP and save to CSV
- [websocket](https://websocket-client.readthedocs.io/en/latest/) - Module for for the client to establish websocket connections
- [tornado](https://www.tornadoweb.org/en/stable/) - Module for server to handle websocket connections

## Getting Started

Server: 
The server runs on port 80
```
$ sudo python3 new_server.py
```

Client:
Open a connection to server on port 80, message size of 1,000,000 bytes, destination websocket.flow-wolf.io, repeat sending of message 100 time, with a wait of 5 seconds between each send:
```
$ python3 client.py -p 80 -s 1000000 -d websocket.flow-wolf.io -r 100 -w 5
```
```
 -d --destination <char> (domain, subdomain, or IP)
 -p --port <integer> (set the port number, defaults to 80 if not specified)
 -r --repeat <integer> (number of times to send the message, defaults to 10 if not specified)
 -s --size <integer> (data payload size in bytes)
 -w --wait <integer> (time interval between messages in seconds, defaults to 1 if not specified)
 -h --help
```


### Authors

flow-wolf
