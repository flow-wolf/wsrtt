import os
import re
import sys
import time
import getopt
import random
import string
import datetime
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
from requests import get
 
 
# Initialise variables
port = ''
repeat = 10
size = 0
data = 0
domain = ''
wait = 1
trackerDict = {}
 
# Define script usage
def usage():
    print('usage:')
    print('-d --destination <char> (domain, subdomain, or IP)')
    print('-p --port <int> (set the port number, defaults to 80 if not specified)')
    print('-r --repeat <int> (number of times to send the message, defaults to 10 if not specified)')
    print('-s --size <int> (message size in bytes)')
    print('-w --wait <int> (time interval between messages in seconds, defaults to 1 if not specified)')
    print('-h --help')
    sys.exit()
 
 
# Get user input
try:
    opts, args = getopt.getopt(sys.argv[1:], 'd:r:s:p:w:h', ['domain=', 'repeat', 'size=', 'port=', 'wait=' ,'help'])
except getopt.GetoptError:
    usage()
 
for opt, arg in opts:
    if opt in ('-h', '--help'):
        usage()
    elif opt in ('-r', '--repeat'):
        try:
            repeat = int(arg)
        except:
            usage()
    elif opt in ('-w', '--wait'):
        try:
            wait = int(arg)
        except:
            usage()
    elif opt in ('-s', '--size'):
        try:
            size = int(arg)
        except:
            usage()
    elif opt in ('-d', '--domain'):
        domain = arg
    elif opt in ('-p', '--port'):
        port = arg
    else:
        usage()
 
# Validate user input of IP or URL
ip_pattern = r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'
url_pattern = r'^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$'
if not re.search(url_pattern,domain) and not re.search(ip_pattern,domain):
    usage()
 
 
# Generate data if defined by user
def gen_chars(size):
    letters = string.ascii_lowercase
    chars = ''.join(random.choice(letters) for i in range(size))
    return chars
if size > 0:
    data = gen_chars(size)
 
 
# Generate file to save results to
ip = get('https://api.ipify.org').content.decode('utf8')
x = datetime.datetime.now()
dateString = x.strftime("%Y-%m-%d_%H-%M")
fileName = "results_" + dateString + ".csv"
f = open(fileName, "a")
f.write("data_size:,"+str(size)+" bytes,\n")
f.write("source:,"+str(ip)+",\n")
f.write("destination:,"+str(domain)+",\n")
f.write("\nmsg_id,c_sent,c_recvd,'rtt time'\n")
 
 
 
def on_message(ws, message):
    msg_pattern = r'^\d+\:\d+.*'
    if re.search(msg_pattern,message):
        c_recv = "%.5f" % round(float(time.time()),5)
        chunks = message.split(':')

        msg_id = chunks[0]
        c_sent = chunks[1]
        msg = chunks[2]

        rtt =  "%.5f" % round(abs(float(c_recv) - float(c_sent)),5)

        key = str(msg_id) + ":" + str(c_sent)
        if trackerDict[key] == msg:
            msg = "{: <10} {: <20} {: <20} {: <20} {: <20}".format(str(msg_id),str(size),str(c_sent),str(c_recv),str(rtt))
            print(msg)
            line = "{},{},{},{}".format(str(msg_id),str(c_sent),str(c_recv),str(rtt))
            f.write("{}\n".format(line))
    else:
        print(message)
 
def on_error(ws, error):
    print(error)
    f.close()
 
def on_close(ws,nill,_):
    print("{}\n{}".format("="*85,"### closed ###"))
    f.close()
 
def on_open(ws):
    def run(*args):
        for i in range(repeat):
            len_i = len(str(i))
            len_to_subtract = len_i + 16 + 2
            modified_data = data[:-len_to_subtract]
            epoch = "%.5f" % round(float(time.time()),5)
            # print("{} {} {} {}".format(len_i, len_epoch, len_data, len_to_subtract))
            # print("{}:{}:{}".format(i,epoch,modified_data))
            ws.send( "{}:{}:{}".format(i,epoch,modified_data) )
            key = str(i)+':'+str(epoch)
            trackerDict[key] = str(modified_data)
            time.sleep(wait)
        ws.close()
        print("thread terminating...")
    thread.start_new_thread(run, ())
 
 
if __name__ == "__main__":
    url = "ws://{}:{}/".format(domain,port)
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(url,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
