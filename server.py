from ctypes import sizeof
import time
import random
import string
import tornado.web
import tornado.ioloop
import tornado.httpserver
import tornado.websocket as ws
from tornado.options import define, options

def gen_chars(size):
    letters = string.ascii_lowercase
    chars = ''.join(random.choice(letters) for i in range(size))
    return chars

 
define('port', default=80, help='port to listen on')
 
class websocket_handler(ws.WebSocketHandler):
    @classmethod
    def route_urls(cls):
        return [(r'/',cls, {}),]
     
    def simple_init(self):
        self.last = time.time()
        self.stop = False
     
    def open(self):
        self.simple_init()
        print("New client connected")
        msg = "{}\n{}\n{: <10} {: <20} {: <20} {: <20} {: <20}\n{}".format("### opened ###","="*85,"msg id","bytes","c_sent","c_recvd","RTT","="*85)
        self.write_message(msg)
 
    def on_message(self, message):
        self.write_message("{}".format(message))
        self.last = time.time()

    def on_close(self):
        print("connection is closed")

    def on_error(ws, error):
        print(error)
     
    def check_origin(self, origin):
        return True
 
def start_server():
    app = tornado.web.Application(websocket_handler.route_urls())
    server = tornado.httpserver.HTTPServer(app)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
 
if __name__ == '__main__':
    start_server()
